package pk.labs.LabB.ui;

import pk.labs.LabB.Contracts.Control;

public interface Negativeable {

	void negative();
}
