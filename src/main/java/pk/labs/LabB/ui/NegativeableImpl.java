/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabB.ui;

import javax.swing.JPanel;
import org.springframework.aop.framework.AopContext;
import pk.labs.LabB.Contracts.Control;

/**
 *
 * @author Adrian
 */
public class NegativeableImpl implements Negativeable {

    @Override
    public void negative() {
        
        Utils utils = new Utils();
        Object proxy =  AopContext.currentProxy();
        JPanel panel = ((Control)proxy).getPanel();
        utils.negateComponent(panel);
        
    }
    
}

