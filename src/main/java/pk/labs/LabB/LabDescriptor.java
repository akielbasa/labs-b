package pk.labs.LabB;

import pk.labs.LabB.Contracts.*;
import pk.labs.LabB.controlpanel.ControlPanelI;
import pk.labs.LabB.display.DisplayI;
import pk.labs.LabB.main.Myjnia;

public class LabDescriptor {
    public static String displayImplClassName = "pk.labs.LabB.display.DisplayI";
    public static String controlPanelImplClassName = "pk.labs.LabB.controlpanel.ControlPanelI";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.IMyjnia";
    public static String mainComponentImplClassName = "pk.labs.LabB.main.Myjnia";
    public static String mainComponentBeanName = "main";
    
    public static String mainComponentMethodName = "Myj";
    public static Object[] mainComponentMethodExampleParams = new Object[] {6,10,"kac"  };
    
    public static String loggerAspectBeanName = "logger";
}
